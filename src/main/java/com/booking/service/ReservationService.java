package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.ServiceRepository;

public class ReservationService {
    private static Scanner input = new Scanner(System.in);
    private static int number = 0;

    public static List<Reservation> createReservation(List<Person> personList, List<Reservation> reservationList){
        // menampilkan semua customer yang dapat melakukan reservasi
        PrintService.showAllCustomer(personList);

        System.out.print("Silahkan Masukkan Customer Id: ");
        String custId = input.nextLine();

        Customer customer = getCustomerById(custId, personList);
        if (customer != null) {
            //  menampilkan semua employee yang dapat melayani reservasi
            PrintService.showAllEmployee(personList);

            System.out.print("Silahkan Masukkan Employee Id: ");
            String empId = input.nextLine();

            Employee employee = getEmployeeById(empId, personList);
            if (employee != null) {
                List<Service> serviceList = ServiceRepository.getAllService();
                List<Service> selectedList = new ArrayList<>();

                boolean looping = true;
                while(looping) {
                    PrintService.showServices(serviceList);

                    System.out.print("Silahkan Masukkan Service Id: ");
                    String servId = input.nextLine();
                    
                    Service service = getServiceById(servId, serviceList);

                    if (service != null) {
                        selectedList.add(service);

                        System.out.print("Tambah service lain? (Y/T): ");
                        String choice = input.nextLine();
                        if (choice.equalsIgnoreCase("t")) looping = false; 
                    } else {
                        System.out.println("Service tidak ditemukan!");
                    }
                    
                }
                
                double totalPriceReservation =  selectedList.stream().mapToDouble(Service::getPrice).sum();
                Reservation reservation = new Reservation(generateIdReservation(), customer, employee, selectedList, totalPriceReservation, "waiting");

                reservationList.add(reservation);

            }

        }

        return reservationList;

    }

    private static String generateIdReservation() {
        number++;
        String formatId = String.format("resv-%02d", number);
        return formatId;
    }

    public static Customer getCustomerById(String customerId, List<Person> personList){
        for (Person person : personList) {
            if (person instanceof Customer && person.getId().equalsIgnoreCase(customerId)) {
                return (Customer) person;
            }
        }

        return null;
    }

    public static Employee getEmployeeById(String employeeId, List<Person> personList) {
        for (Person person: personList) {
            if (person instanceof Employee && person.getId().equalsIgnoreCase(employeeId)) {
                return (Employee) person;
            }
        }

        return null;
    }

    public static Service getServiceById(String serviceId, List<Service> serviceList) {
        for (Service service: serviceList) {
            if (service.getServiceId().equalsIgnoreCase(serviceId)) {
                return service;
            }
        }

        return null;
    }

    public static void editReservationWorkstage(List<Reservation> reservationList){
        
    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
